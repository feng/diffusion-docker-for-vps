#!/usr/bin/env python3.9

import os
import argparse


'''
    # TODO: enhance pipeline with the following args

    # diffusion
    parser.add_argument( "--model", type=str, default="bes-dev/stable-diffusion-v1-4-openvino", help="model name")
    parser.add_argument( "--seed", type=int, default=None, help="random seed for generating consistent images per prompt")
    parser.add_argument( "--beta-start", type=float, default=0.00085, help="LMSDiscreteScheduler::beta_start")
    parser.add_argument( "--beta-end", type=float, default=0.012, help="LMSDiscreteScheduler::beta_end")
    parser.add_argument( "--beta-schedule", type=str, default="scaled_linear", help="LMSDiscreteScheduler::beta_schedule")
    parser.add_argument( "--num-inference-steps", type=int, default=32, help="num inference steps")
    parser.add_argument( "--guidance-scale", type=float, default=7.5, help="guidance scale")
    parser.add_argument( "--eta", type=float, default=0.0, help="eta")
    parser.add_argument( "--tokenizer", type=str, default="openai/clip-vit-large-patch14", help="tokenizer")
    parser.add_argument( "--prompt", type=str, default="Street-art painting of Emilia Clarke in style of Banksy, photorealism", help="prompt")
    parser.add_argument( "--init-image", type=str, default=None, help="path to initial image")
    parser.add_argument( "--strength", type=float, default=0.5, help="how strong the initial image should be noised [0.0, 1.0]")
    parser.add_argument( "--output", type=str, default="output.png", help="output image name")

    # realesrgan
'''

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument( "--prompt", type=str, default="Street-art painting of Emilia Clarke in style of Banksy, photorealism", help="prompt")
    args = parser.parse_args()
    prompt = args.prompt

    prompt = prompt.replace( "'", " " )
    prompt = prompt.replace( '"', " " )

    os.makedirs( '/app/experiments/pretrained_models', exist_ok=True )
    realesrgan_path = '/app/experiments/pretrained_models/RealESRGAN_x4plus.pth'
    if not os.path.isfile( realesrgan_path ):
        print( f'RealESRGAN model is not found from {realesrgan_path}. Download it.' )
        os.system( f'wget  https://github.com/xinntao/Real-ESRGAN/releases/download/v0.1.0/RealESRGAN_x4plus.pth -O {realesrgan_path}' )

    gfpgan_path = '/app/experiments/pretrained_models/GFPGANv1.3.pth'
    if not os.path.isfile( gfpgan_path ):
        print( f'GFPGAN model is not found from {gfpgan_path}. Download it.' )
        os.system( f'wget https://github.com/TencentARC/GFPGAN/releases/download/v1.3.0/GFPGANv1.3.pth -O {gfpgan_path}' )


    #diffusion_command = f'python3.9 /app/inference_stable_diffusion.py --prompt "{prompt}" --num-inference-steps 32 --output /results/result.png'
    diffusion_command = f'python3.9 /app/inference_stable_diffusion.py --prompt "{prompt}" --num-inference-steps 128 --output /results/result.png'
    print( f'Executing diffuser command {diffusion_command}' )
    os.system( diffusion_command )

    realsrgan_command = f'python3.9 /app/inference_realesrgan.py --input /results/result.png --output /results --suffix 4x --fp32 --face_enhance  --outscale 4 --alpha_upsampler realesrgan'
    print( f'Enhance diffusion image with command: {realsrgan_command}' )
    os.system( realsrgan_command )

if __name__ == '__main__':
    main()



