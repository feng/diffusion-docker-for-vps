# Diffusion Pipeline

Build the docker:

```bash
git clone https://codeberg.org/feng/diffusion-docker-for-vps.git
cd diffusion-docker-for-vps
docker build --file Dockerfile . -t  diffusion
```

Generate image with prompt (first time could be slow, 32 iterations, peak memory usage 8.3 GB):

```bash
docker run -it -v $(pwd)/results:/results -v $(pwd)/gfpgan_cache:/app/gfpgan -v $(pwd)/cache:/app/experiments/ -v $(pwd)/hf_cache:/root/.cache/huggingface diffusion  --prompt 'britney spears,, intricate, highly detailed, green skin!, digital painting, artstation, concept art, smooth, sharp focus, illustration, art by artgerm and greg rutkowski and alphonse mucha'
```

Generated image will be placed at `$(pwd)/results/result_4x.png`.

Generate another image by passing a new prompt:

```bash
docker run -it -v $(pwd)/results:/results -v $(pwd)/gfpgan_cache:/app/gfpgan -v $(pwd)/cache:/app/experiments/ -v $(pwd)/hf_cache:/root/.cache/huggingface diffusion  --prompt 'Portrait tzuyu from twice + black hair of futuristic female police, black armored uniform, at rooftop futuristic colorpunk tokyo rainy night, ssci - fi and fantasy, intricate and very very very beautiful, highly detailed, digital painting, artstation, concept art, smooth and sharp focus, illustration, art by tian zi and wlop and alphonse mucha'
```

