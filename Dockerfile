FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
    python3.9-dev python3.9-distutils \
    wget libgl1 libglib2.0-0 && \
    wget https://bootstrap.pypa.io/get-pip.py -O get-pip.py && \
    python3.9 get-pip.py && \
    pip3 install --upgrade pip

WORKDIR /app
COPY requirements.txt /app/

RUN pip3 install -r requirements.txt

COPY inference_realesrgan.py inference_stable_diffusion.py pipeline.py stable_diffusion_engine.py /app/
RUN chmod +x /app/pipeline.py

ENTRYPOINT ["python3", "/app/pipeline.py", "--prompt", "some prompt"]

